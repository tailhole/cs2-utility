# Content
1. [Mid](#mid)
    - [Short](#short)
    - [Window](#window)
2. [B_Site](#b)
    - [Bench](#bench)
    - [Van](#van)
    - [Right_Arch](#rarch)
    - [Left_Arch](#larch)
3. [A_Site](#a)
    - [Stairs](#stairs)
# Trashcan smokes <a name="mid"></a>
### (tuck in this corner against trashcan. do not jump on top of it)
![screenshot_001.jpg](_resources/screenshot_001.jpg)
## Top Mid / Short (Normal Left-Click) <a name="short"></a>
![screenshot_005.jpg](_resources/screenshot_005.jpg)
## Window (Crouch Jump-Throw) <a name="window"></a>
### Crouch in the corner and line your cursor in this area
![screenshot_002.jpg](_resources/screenshot_002.jpg)
### Stay crouched and walk forward, jump-throwing when your cursor gets to about here:
![screenshot_003.jpg](_resources/screenshot_003.jpg)
### Note that you want to be throwing just before it reaches the edge of [archetecture term]

# Behind Apartments (B Smokes) <a name="b"></a>
### Line yourself up against the back wall using this dark slope as your guide
![screenshot_006.jpg](_resources/screenshot_006.jpg)
### Once centered, there are 2 good jump-throw smokes from this position:
## Bench (Jump-Throw)<a name="bench"></a>
![screenshot_007.jpg](_resources/screenshot_007.jpg)
## Van (Jump-Throw) <a name="van"></a>
![screenshot_008.jpg](_resources/screenshot_008.jpg)
### (lands on top of the van. make sure your crosshair isn't too high or your smoke will go to narnia)
## Arches 
### Line yourself up with the middle of this pillar
![screenshot_009.jpg](_resources/screenshot_009.jpg)
### Left (Jump-Throw)
![screenshot_011.jpg](_resources/screenshot_011.jpg) <a name="larch"></a>
### Right (Jump-Throw) <a name="rarch"></a>
![screenshot_013.jpg](_resources/screenshot_013.jpg)

# A Site <a name="a"></a>
## Stairs (Left-Click) <a name="stairs"></a>
### Jump up on the first step and align yourself with the center of this pillar
![stairs1.jpg](_resources/stairs1.jpg)
### Find the middle of the dark spot on this piece of scaffolding
![stairs2.jpg](_resources/stairs2.jpg)
### Move laterally so that you are aligned with both the dark spot and the corner of wood below your crosshair--left-click throw 
![stairs3.jpg](_resources/stairs3.jpg)

